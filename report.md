| Table Progeress report |

| Suyunboy Yuldoshev       | Stage   | Start date | End date | Comment |
|---------|---------|------------|----------|---------|
| 1. Task Clarification  | Recieve and analyze the task | 11 March 11:30     |   11 March 17:30 | Many misundesrtandinga  | 
| 2. Task Clarification  |  Clarify details and requirements with the mentor | 12 March 17:00     |   |  We haven't clarified the details yet | 
| 3. Task Clarification  |   Propose a 10-week work plan for course project development | 15 March 08:30     |   |   | 
| 4. Task Clarification  |  Create a progress report and place it in your repository | 15 March   9:00   |   |   | 
| --- | --- | --- | ---| --- |
| 1. Analysis  |  Study the applied area by the individual task |  18 March 15:00     |   |   | 
| 2. Analysis  |  Create a progress report and place it in your repository | 18 March 19:00   |   |   | 
| 3. Analysis  |  Define and describe the basic functionality that needs to be implemented first (MVP—minimum viable product) | 19 March 17:30     |   |   | 
| 4. Analysis  |  Describe additional functionality for improving usability, security, performance, etc. | 20 March 11:00     |   |   | 
| 5. Analysis  | Describe any advanced functionality that might be useful in the future | 20 March 15:00     |   |   | 
| --- | --- | --- | ---| --- |
| 1. Use Cases  | Describe several options for using the program (how the program should behave from the user's point of view) however you prefer. | 21 March 09:00     |   |   |